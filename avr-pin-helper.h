/*
 * avr_helper.h
 *
 * Created: 10/10/2019 3:49:51 PM
 *  Author: d
 */ 



#define HIGH 0x01;
#define LOW  0x00;


void makePinHigh(uint8_t *yourPort, uint8_t value)
{
	*yourPort |= (0x01 << value);
}

void makePinLow(uint8_t *yourPort, uint8_t value)
{
	*yourPort &=  (~(0x01 << value));
}

void togglePin(uint8_t *yourPort, uint8_t value)
{
	*yourPort ^= (1 << value);
}

void makePinOutput(uint8_t *portRegister, uint8_t pin)
{
	*portRegister |= 0x01 << pin;
}

void makePinInput(uint8_t *portRegister, uint8_t pin)
{
	*portRegister &=  (~(0x01 << pin));
}

uint8_t readPin(uint8_t *yourPort, uint8_t pin)
{
	return ((*yourPort >> pin) & 0x01);
}
