#define OCR0A_VAL 129

//OCR0 129 AND TCCR0B 5 (101 THAT MEANS 1024, check doc)

//one per sec timer initialization example
void setupTimer0(void)
{
	TCCR0A = (1<<WGM01);
	OCR0A  = OCR0A_VAL;
	TIMSK0 = (1<<OCIE0A);
	TCCR0B = (5<<CS00);
}


// (16,000,000 HZ) / (2 * 1024 * 60) -1 = 129
//1024 is TCCR0B = (5<<CS00);
//60 represents; how many time would you like to run this function per second
//16mhz should be defined via #define F_CPU 16000000UL and you might need 16mhz crystal


/************************************************************************
 

short interruptCount = 0;

ISR(TIMER0_COMPA_vect)
{
	interruptCount++;
	if(interruptCount == OCR0A)
	{
		togglePin(&PORTB,PINB1);
		interruptCount = 0;
	}

}
                                                                     
************************************************************************/
